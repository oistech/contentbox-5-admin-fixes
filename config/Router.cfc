component {
	function configure(){
		var moduleEntryPoint = "cbAdmin5Fixes";


		route( "cbadmin/module/#moduleEntryPoint#/home/:moduleAction" )
			.rcAppend( { moduleHandler : "home" } )
			.to( "contentbox-admin:modules.execute" );

		route( "/:handler?/:action?" )
			.withAction( "notFoundFacade" )
			.toHandler( "home" ).end();

	}
}
