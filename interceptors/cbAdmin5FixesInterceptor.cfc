component extends="coldbox.system.Interceptor" {

	property name="settingService"  inject="settingService@contentbox";
	property name="CBHelper"        inject="id:CBHelper@contentbox";

    function configure(){
        __updateSettings();
    } // configure()

    public function cbadmin_afterContent( event, interceptData, buffer, rc, prc ){

        

        prc.cbAdmin5FixesSettings = __updateSettings();
        savecontent variable="oisAdminCustomization" {
            include "inc/oisAdminCustomization.cfm";
        };
        arguments.buffer.append( oisAdminCustomization );
    } // cb_cbAdmin5Fixes_submitted()

    private function __updateSettings(){
		// Load default module settings
		var moduleSettings = CBHelper.getModuleSettings("cbAdmin5Fixes");
        var oSettings = settingService.findWhere( criteria = { name : "cbAdmin5Fixes" } );
		if( isNull( oSettings ) ){
			oSettings = settingService.new( properties = { name : "cbAdmin5Fixes" } );
		}
        if( isJson( oSettings.getValue() ) ){
            var cbModuleSettings = deserializeJson( oSettings.getValue() );
        }else{
            var cbModuleSettings = {};
        }
        // update moduleSettings struct from ContentBox settings service data
		for ( var currentKey in cbModuleSettings ){ 
            moduleSettings[ currentKey ] = cbModuleSettings[ currentKey ];
		} 
        // update propery values
        for ( var currentKey in moduleSettings ) { 
            setProperty( currentKey, moduleSettings[ currentKey ] );
        }
        return moduleSettings;
    } // updateSettings()

	/**
	 * Get Real IP, by looking at clustered, proxy headers and locally.
	 */
	private function __getRealIP(){
		var headers = getHTTPRequestData( false ).headers;

		// When going through a proxy, the IP can be a delimtied list, thus we take the last one in the list
		if ( structKeyExists( headers, "x-cluster-client-ip" ) ) {
			return trim( listLast( headers[ "x-cluster-client-ip" ] ) );
		}
		if ( structKeyExists( headers, "X-Forwarded-For" ) ) {
			return trim( listFirst( headers[ "X-Forwarded-For" ] ) );
		}
		return len( cgi.remote_addr ) ? trim( listFirst( cgi.remote_addr ) ) : "127.0.0.1";
	}

    /* 
        onInvalidEvent: checks to see if the interceptData.invalidEvent was pointing to this module and if so, overides. 
        Used to catch any root url uncaught errors, such as /cbAdmin5Fixes/handerNameThatDoesNotExists
        if root url is /cbAdmin5Fixes/home/actionThatDoesNotExist it is caught by the onMissingAction() in home.cfc handler
        acutal hander.event combinations from root are caught by Router.cfc and routed to home.notFoundFacade
    */
    public function onInvalidEvent( event, interceptData ){
        if( interceptData.keyExists("invalidEvent") ){
            if( left( interceptData.invalidEvent, len("#cbhelper.getmoduleService().getModuleConfigCache().cbAdmin5Fixes.modelNamespace#:") ) == "#cbhelper.getmoduleService().getModuleConfigCache().cbAdmin5Fixes.modelNamespace#:" ){
                interceptData.ehBean.setModule( cbhelper.getmoduleService().getModuleConfigCache().cbAdmin5Fixes.modelNamespace );
                interceptData.ehBean.setHandler("home");
                interceptData.ehBean.setMethod("notFoundFacade");
                interceptData.override = true;
            }
        }
    }

}  