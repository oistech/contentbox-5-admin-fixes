<cfoutput>
    <!--- CHECK IF WE ARE ON THE CBADMIN MENU EDITOR ROUTE --->
    <cfif   prc.keyExists("currentRouteRecord") 
            AND prc.currentRouteRecord.MODULE EQ "contentbox-admin" 
            AND prc.currentRouteRecord.HANDLER EQ "menus" 
            AND prc.currentRouteRecord.ACTION EQ "editor"
            AND listFirst( prc.cbsettings.cb_version, "." ) EQ 5
            AND ( booleanFormat( prc.cbAdmin5FixesSettings.fixContentSelector ) OR booleanFormat( prc.cbAdmin5FixesSettings.fixMediaSelector ) ) >
        <script>
            var mutationObserver = new MutationObserver( function(mutations) {
                mutations.forEach( function( mutation ){
                    // <cfif booleanFormat( prc.cbAdmin5FixesSettings.fixContentSelector ) >
                    if( $( mutation.target ).find('.select-content').length > 0 ){
                        $( mutation.target ).find('.select-content').each(function( index ) {
                            var onClickAttached = $( this ).data( "onclickattached" );
                            if( onClickAttached === undefined ){
                                $( this ).prop("onclick", null).off("click");
                                $( this ).on( 'click', function() {
                                    input = $( this ).siblings( 'input[name^=contentTitle]' );
                                    hidden= $( this ).siblings( 'input[name^=contentSlug]' );
                                    label = $( this ).closest( '.dd3-extracontent' ).find( 'input[name^=label]' );
                                    typeIcon = $( this ).closest( '.dd3-item' ).find( '.dd3-type' );
                                    openRemoteModal( '#event.buildLink( "#prc.cbAdminEntryPoint#.content.showRelatedContentSelector" )#', { contentType: 'Page,Entry' }, 900, 600 );
                                });
                                $( this ).data( "onclickattached", "true" );
                                // console.log( "Attaching onClick Event Handler!" );
                            }
                        });
                    }
                    // </cfif>
                    // <cfif booleanFormat( prc.cbAdmin5FixesSettings.fixMediaSelector ) >
                        if( $( mutation.target ).find('.select-media').length > 0 ){
                            $( mutation.target ).find('.select-media').each(function( index ) {
                                var onClickAttached = $( this ).data( "onclickattached" );
                                if( onClickAttached === undefined ){
                                    $( this ).prop("onclick", null).off("click");
                                    $( this ).on( 'click', function() {
                                        input = $( this ).siblings( 'input[name^=media]' );
                                        hidden= $( this ).siblings( 'input[name^=mediaPath]' );
                                        label = $( this ).closest( '.dd3-extracontent' ).find( 'input[name^=label]' );
                                        typeIcon = $( this ).closest( '.dd3-item' ).find( '.dd3-type' );
                                        win = window.open( "#wirebox.getInstance( "coldbox:requestService" ).getContext().buildLink( to = "cbadmin.menus.filebrowser" )#", 'fbSelector', 'height=600,width=600' ) 
                                    });
                                    $( this ).data( "onclickattached", "true" );
                                    // console.log( "Attaching onClick Event Handler!" );
                                }
                            });
                        }
                    // </cfif>

                });
            });
            mutationObserver.observe( document.querySelector( "##nestable.dd" ), {
                attributes: false, characterData: false, childList: true, subtree: true, attributeOldValue: false, characterDataOldValue: false
            });
        </script>
    </cfif>
</cfoutput>