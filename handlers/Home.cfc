component {

	property name="settingService"		inject="id:settingService@contentbox";
   	property name="cbMessagebox" 	    inject="messagebox@cbmessagebox";
	property name="markdown" 			inject="Processor@cbmarkdown";
	property name="themeService" 		inject="themeService@contentbox";
	property name="cbHelper" 			inject="provider:CBHelper@contentbox";
	property name="ormService"			inject="provider:entityservice";

	function preHandler( event, rc, prc, action, eventArguments ){
		// Only allow viewing from the /cbadmin/module/cbAdmin5Fixes/:hander/:action route
		var allowedNonAdminActions = [ "notAllowed", "notFoundFacade" ];
		if( listFirst( event.getContext().event, ":" ) != "contentbox-admin" ){
			if( !allowedNonAdminActions.findNoCase( arguments.action ) ){
				event.overrideEvent( 'cbAdmin5Fixes:home.notFoundFacade' );
			}
		}
	} // preHandler()

	function index(event,rc,prc){
		prc.saveURL	= prc.CBHelper.buildModuleLink("cbAdmin5Fixes","home.saveSettings");
		prc.cbAdmin5Fixes = prc.cbhelper.getmoduleService().getModuleConfigCache().cbAdmin5Fixes;
		prc.themeService = themeService;
		// Load default module settings
		var moduleSettings = __moduleSettings( event, rc, prc );		
		event.setView( view= "home/index" );
	} // settings()

	function saveSettings( event, rc, prc ){
		// read the module settings
		var moduleSettings = prc.CBHelper.getModuleSettings("cbAdmin5Fixes");
		// get the ContentBox Settings from settingService
        var oSettings = settingService.findWhere( criteria = { name : "cbAdmin5Fixes" } );
		if( isNull( oSettings ) ){
			oSettings = settingService.new( properties = { name : "cbAdmin5Fixes" } );
		}
		// Get modules settings from ContentBox settingService
        var cbModuleSettings = !isNull( oSettings ) ? deserializeJson( oSettings.getValue() ) : {};
		// update moduleSettings struct from current ContentBox settings service data
		for ( var currentKey in moduleSettings ){ 
            if( rc.keyExists( currentKey ) ){
                moduleSettings[ currentKey ] = rc[ currentKey ];
            }else if( cbModuleSettings.keyExists( currentKey ) ){
				moduleSettings[ currentKey ] = cbModuleSettings[ currentKey ];
			}
		}
		// Save the settings
		var oSettings = settingService.findWhere( criteria = { name : "cbAdmin5Fixes" } );
		oSettings.setValue( serializeJSON( moduleSettings ) )
		settingService.save( oSettings );
		settingService.flushSettingsCache();
		// Set Messagebox
		cbMessageBox.info("Settings Saved & Updated!");
		// Relocate with CB Helper
		prc.CBHelper.setNextModuleEvent( "cbAdmin5Fixes", "home" );
	} // saveSettings()

	function about( event, rc, prc ){
		// Load readme.md and convert to html then output on about page
		var readme_path = prc.cbhelper.getmoduleService().getmoduleRegistry().cbAdmin5Fixes.PHYSICALPATH & "/cbAdmin5Fixes/README.md";
		var readme_contents = fileRead( readme_path );
		prc.readme_html = variables.markdown.toHTML( readme_contents );
		event.setView( view= "home/about" );
	} // about()

	function notAllowed( event, rc, prc ){
		return { "success" : false, "message" : "Not Allowed" };
	} // notAllowed()

	private function __moduleSettings( event, rc, prc ){
		// read the module settings
		var moduleSettings = CBHelper.getModuleSettings("cbAdmin5Fixes");
		// get the ContentBox Settings from settingService
        var oSettings = settingService.findWhere( criteria = { name : "cbAdmin5Fixes" } );
		if( isNull( oSettings ) ){
			oSettings = settingService.new( properties = { name : "cbAdmin5Fixes" } );
		}
		// Get modules settings from ContentBox settingService
        var cbModuleSettings = !isNull( oSettings ) ? deserializeJson( oSettings.getValue() ) : {};
		// update moduleSettings struct from current ContentBox settings service data
		for ( var currentKey in moduleSettings ){ 
            if( cbModuleSettings.keyExists( currentKey ) ){
				moduleSettings[ currentKey ] = cbModuleSettings[ currentKey ];
				event.setValue( currentKey, cbModuleSettings[ currentKey ] );
			}else{
				event.setValue( currentKey, moduleSettings[ currentKey ] );
			}
		}
		return moduleSettings;
	} // __moduleSettings()  

	/*
		Uses the standard ContentBox page layout and notFound view to mimic the "oopsy" page (404)
		Called From: 
			home.preHandler: 
				request is not comming from /cbadmin/module/cbAdmin5Fixes/ (withing the ContentBox Admin)
			home.onMissingAction:
				handler was correct (home) but action wasn't found
			themeSettingsInterceptor.onInvalidEvent:
				catches module invalid events ( I.E. /cbAdmin5Fixes/handerNameThatDoesNotExists )
	*/
	function notFoundFacade( event, rc, prc ){
		cbHelper.prepareUIRequest();
		event.setPrivateValue( "missingPage", prc.currentRoutedURL );
		rc.layoutName = "#themeService.getActiveTheme().name#/layouts/pages";
		rc.module = themeService.getActiveTheme().module;
		rc.view = "#themeService.getActiveTheme().name#/views/notFound";
		event
			.setLayout( name = "#themeService.getActiveTheme().name#/layouts/pages", module = themeService.getActiveTheme().module )
			.setView( view = "#themeService.getActiveTheme().name#/views/notFound", module = themeService.getActiveTheme().module  );
	} // notFoundFacade()

	function onMissingAction( event, rc, prc, missingAction, eventArguments ){
		event.overrideEvent( 'cbAdmin5Fixes:home.notFoundFacade' );
	} // onMissingAction()

}