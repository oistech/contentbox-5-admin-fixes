<cfoutput>
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 style="margin: 10px 0;">
                        <i class="fas fa-info"></i> OIS Simple Contact Form
                    </h3>
                </div>
                <div class="panel-body">
                    <!--- MAIN PANEL CONTENTS --->
                    <div class="row">
                            <div class="col-md-12" id="oisSimpleFormContactWrapper">
                                #prc.readme_html#
                            </div>
                        </div>
                    <!--- /MAIN PANEL CONTENTS --->
                </div><!--- /panel-body --->
            </div>
        </div>
        <div class="col-md-3">
            <cfinclude template="inc/sidePanel_logo.cfm">
            <cfinclude template="inc/sidePanel_actions.cfm">
            <cfinclude template="inc/sidePanel_assistance.cfm">
        </div>
    </div>
</cfoutput>
<!--- hide first h1 since heading is in the Panel with button --->
<script>
    document.addEventListener( "DOMContentLoaded", () => {
        $( "#oisSimpleFormContactWrapper h1:first" ).hide();
        // Fix Anchor links because of issue with having base tag in header 
        // limit to only links inside the oisSimpleFormContactWrapper div so we don't break the cbadmin menu
        $("#oisSimpleFormContactWrapper a[href^='\#']").each(function() {
            this.href = location.href.split("#")[0] + '#' + this.href.substr(this.href.indexOf('#')+1);
        });
    });
</script>