<cfoutput>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">
                <i class="fa fa-question"></i> Need Assistance?
            </h3>
        </div>
        <div class="panel-body">
            <p>You can view the help information using the button below or contact me for help at <a href="mailto:mrigsby@oistech.com">mrigsby@oistech.com</a></p>
            <a href="#prc.CBHelper.buildModuleLink("cbAdmin5Fixes","home.about")#" class="btn btn-info" style="color: white;">
                <i class="fas fa-question"></i> Help
            </a>
        </div>
    </div>
</cfoutput>