<cfoutput>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="fas fa-mouse"></i> Links</h3>
        </div>
        <div class="panel-body" style="text-align: center;">
            <div class="btn-group btn-group-sm">
                <a href="#prc.CBHelper.buildModuleLink("cbAdmin5Fixes","home")#" class="btn btn-warning" style="color: white;">
                    <i class="fas fa-cog"></i> Settings
                </a> 
                <a href="#prc.CBHelper.buildModuleLink("cbAdmin5Fixes","home.about")#" class="btn btn-info" style="color: white;">
                    <i class="fas fa-question"></i> Help
                </a>

            </div>
        </div>
    </div>
</cfoutput>