<cfoutput>
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 style="margin: 10px 0;">
                        <i class="fa fa-screwdriver"></i> OIS ContentBox 5 Admin Fixes
                    </h3>
                </div>
                <div class="panel-body">
                    <!--- Messageboxes --->
                    #cbMessageBox().renderit()#
                    <!--- MAIN PANEL CONTENTS --->
                    <form id="cbAdmin5Fixes-settings-form" method="post" action="#prc.saveURL#" role="form">

                        <!--- MENU MANAGER FIXES --->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fas fa-bars"></i> Menu Manager Fixes</h3>
                            </div>
                            <div class="panel-body">
                                
                                <div class="row">
                                
                                    <div class="col-md-6">
                                         #html.select(
                                            name    		= "fixContentSelector",
                                            selectedValue   = event.getValue('fixContentSelector'),
                                            label   	    = '<i class="fas fa-box"></i> Enable Content Selector Fix',
                                            class   	    = "form-control",
                                            wrapper 	    = "div class=controls",
                                            labelClass 	    = "control-label",
                                            groupWrapper    = "div class=form-group",
                                            options         = [ { "name": "Yes", "value" : "1" }, { "name": "No", "value" : "0" } ]
                                        )#
                                    </div>
                                
                                    <div class="col-md-6">
                                         #html.select(
                                            name    		= "fixMediaSelector",
                                            selectedValue   = event.getValue('fixMediaSelector'),
                                            label   	    = '<i class="fas fa-photo-video"></i> Enable Media Selector Fix',
                                            class   	    = "form-control",
                                            wrapper 	    = "div class=controls",
                                            labelClass 	    = "control-label",
                                            groupWrapper    = "div class=form-group",
                                            options         = [ { "name": "Yes", "value" : "1" }, { "name": "No", "value" : "0" } ]
                                        )#
                                    </div>
                                
                                </div>

                            </div><!--- / panel-body --->
                        </div>
                        <!--- / ALERT SETTINGS --->

                        
                        <div class="row" style="margin-top: 20px;">            
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-success btn-send">Save Settings</button>
                            </div>
                        </div>

                    </form>
                    <!--- /MAIN PANEL CONTENTS --->
                </div><!--- /panel-body --->
            </div>
        </div>
        <div class="col-md-3">
            <cfinclude template="inc/sidePanel_logo.cfm">
            <cfinclude template="inc/sidePanel_actions.cfm">
            <cfinclude template="inc/sidePanel_assistance.cfm">
        </div>
    </div>
</cfoutput>