component {

	this.title 					= "ContentBox 5 Admin Fixes";
	this.author 				= "Michael Rigsby <mrigsby@oistech.com>";
	this.webURL 				= "http://www.oistech.com";
	this.description 			= "Fixes a couple ContentBox 5 Admin issues with intetcetors and JS";
	this.version				= "1.0.0";
	this.viewParentLookup 		= true;
	this.layoutParentLookup 	= true;
	this.entryPoint				= "cbAdmin5Fixes";
	this.inheritEntryPoint 		= false;
	this.modelNamespace			= "cbAdmin5Fixes";
	this.cfmapping				= "cbAdmin5Fixes";
	this.autoMapModels			= true;
	this.dependencies       	= [ "contentbox-admin" ];

	// admin menu settings used below
	this.OISADMIN_TOPMENU		= "oisadminmenu";
	this.OISADMIN_TOPMENU_LABEL = "<i class='fas fa-laptop-code'></i> OIS Modules";

	this.OISADMIN_SUBMENU		= "cbAdmin5FixesHome";
	this.OISADMIN_SUBMENU_LABEL	= '<i class="fa fa-screwdriver" style="margin-right: 0;"></i> CB 5 Admin Fixes';

	function configure(){
		parentSettings = {
		};
		settings = {
			fixContentSelector	: "1",
			fixMediaSelector	: "1"
		};
		interceptorSettings = {
			customInterceptionPoints = []
		};
	} // configure()

	function onLoad(){
		manageAdminMenu("add");
		controller.getInterceptorService().registerInterceptor("#moduleMapping#.interceptors.cbAdmin5FixesInterceptor");
	} // onLoad()

	function onActivate(){
		// store default settings
		var settingService = controller.getWireBox().getInstance("SettingService@contentbox");
		var setting = settingService.findWhere( criteria={ name="#this.modelNamespace#" } );
		if( isNull( setting ) ){
			var oisThemeSettingsSettings = settingService.new( properties={ name="#this.modelNamespace#", value=serializeJSON( settings ) } );
			settingService.save( oisThemeSettingsSettings );
		}
	} // onActivate()

	function onUnload(){
		manageAdminMenu("remove");
		controller.getInterceptorService().unregister( interceptorName="cbAdmin5FixesInterceptor" );
	} // onUnload()

	function onDeactivate(){
		// var settingService = controller.getWireBox().getInstance("SettingService@contentbox");
		// var setting = settingService.findWhere( criteria={ name="#this.modelNamespace#" } );
		// if( !isNull( setting ) ){
		// 	settingService.delete( setting );
		// }
	} // onDeactivate()

	function manageAdminMenu( action = "add" ){
		var menuService = controller.getWireBox().getInstance( "AdminMenuService@contentbox" );
		var topMenuMap = menuService.getPropertyMixin( "topMenuMap" );
		
		if( action == "add" ){
		
			if( !topMenuMap.keyExists( this.OISADMIN_TOPMENU ) ){
				menuService.addTopMenu(
					name	= this.OISADMIN_TOPMENU,
					label	= this.OISADMIN_TOPMENU_LABEL,
					href	= "##"
				);
			}

			menuService.addSubMenu( 
				topMenu	= this.OISADMIN_TOPMENU, 
				name	= this.OISADMIN_SUBMENU, 
				label	= this.OISADMIN_SUBMENU_LABEL,
				href	= "#menuService.buildModuleLink( "#this.modelNamespace#", 'home' )#"
			);

		}else{
			// Anythine value passed for action other than "add" will result in removing the menu

			menuService.removeSubMenu( 
				topMenu	= this.OISADMIN_TOPMENU, 
				name	= this.OISADMIN_SUBMENU
			);

			if( topMenuMap.keyExists( this.OISADMIN_TOPMENU ) ){
				if( !topMenuMap[ this.OISADMIN_TOPMENU ].subMenu.len() ){
					menuService.removeTopMenu( topMenu	= this.OISADMIN_TOPMENU );
				}
			}
			
		}

	} // manageAdminMenu()

}