# ContentBox 5 Admin Fixes

## About
The ContentBox 5 Admin Fixes was developed by [Michael Rigsby, Owner of OIS Technolgies](https://www.oistech.com) to address some minor issues
with the ContentBox 5 Admin. 

### Menu Manager

The ContentBox 5 Menu Manager Content and Media Selectors on click events had a bug preventing the selector modal and window from opening when 
the corresponding buttons where clicked. You are able to enable the fix for the Content Selector and/or the Media Selector in the module settings. 

Since the Menu Manager loads the form elements for each of these the fix utilizes a [javascript DOM Mutation Observer](https://developer.mozilla.org/en-US/docs/Web/API/MutationObserver) to watch for the content or media selector being added to the DOM and removes any existing onclick events and adds the proper corresponding onclick event handler. It tracks if the onclick has been attached by the module using the jquery data api to prevent attaching multiple onclick events to the same selector.

### Support
Any questions, input or comments can be emailed to [mrigsby@oistech.com](mailto:mrigsby@oistech.com)
